﻿Shader "Custom/DistortedWater" {
	Properties{
		_Colour("Colour", Color) = (1,1,1,1)
		_SpecColour("Specular Colour", Color) = (1,1,1,1)
		_MainTex("MainTex (RGB)", 2D) = "white" {}
		[NoScaleOffset] _FlowMap("Flow (RG), A noise", 2D) = "black" {}
		[NoScaleOffset] _NormalMap("Normals", 2D) = "bump" {}
		_UJump("U jump per phase", Range(-0.25, 0.25)) = 0.25
		_VJump("V jump per phase", Range(-0.25, 0.25)) = 0.25
			_Tiling("Tiling", Float) = 1
			_Speed("Speed", Float) = 1
			_FlowStrength("Flow Strength", Float) = 1
				_FlowOffset("Flow offset", Float) = 0
			_SpecPower("Specular Power", Float) = 1
	}
		SubShader{
			Tags { "RenderType" = "Opaque"}
			LOD 200


			Pass{

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			#include "AutoLight.cginc"
			#include "Lighting.cginc"

			float4 _Colour;
			float4 _SpecColour;

			sampler2D _MainTex;
			sampler2D _FlowMap;
			sampler2D _NormalMap;

			float _UJump;
			float _VJump;
			float _Tiling;
			float _Speed;
			float _FlowStrength;
			float _FlowOffset;

			float _SpecPower;

			float3 FlowUVW(float2 uv, float2 flowVector, float2 jump, float flowOffset, float tiling, float time, bool flowB) {
				float offset = flowB ? 0.5 : 0;
				float progress = frac(time + offset);
				float3 uvw;
				uvw.xy = uv - flowVector * (progress + flowOffset);
				uvw.xy *= tiling;
				uvw.xy += offset;
				uvw.z = 1 - abs(1 - 2 * progress);
				return uvw;
			}

		struct appdata {
			float4 position : POSITION;
			float2 texCoords : TEXCOORD0;
		};

		struct v2f {
			float4 position : SV_POSITION;
			float2 texCoords : TEXCOORD0;
			float3 viewDir : TEXCOORD1;
		};

		v2f vert(appdata v) {
			v2f o;

			o.position = UnityObjectToClipPos(v.position);
			o.texCoords = v.texCoords;
			o.viewDir = normalize(UnityWorldSpaceViewDir(mul(unity_ObjectToWorld, v.position)));

			return o;
		}

		float4 frag(v2f i) : SV_Target{

			float2 flowVector = tex2D(_FlowMap, i.texCoords).rg * 2 - 1;
			flowVector *= _FlowStrength;
			float noise = tex2D(_FlowMap, i.texCoords).a;
			float time = _Time.y * _Speed + noise;
			float2 jump = float2(_UJump, _VJump);

			float3 uvwA = FlowUVW(i.texCoords, flowVector, jump, _FlowOffset, _Tiling, time, false);
			float3 uvwB = FlowUVW(i.texCoords, flowVector, jump, _FlowOffset, _Tiling, time, true);

			float3 normalA = UnpackNormal(tex2D(_NormalMap, uvwA.xy)) * uvwA.z;
			float3 normalB = UnpackNormal(tex2D(_NormalMap, uvwB.xy)) * uvwB.z;

			// combine the normal from a and b.
			/*float3 normal = normalize(normalA + normalB);

			float ndotl = max(0, dot(normal, _WorldSpaceLightPos0));

			float3 halfVector = normalize(_WorldSpaceLightPos0 + i.viewDir);
			float ndoth = max(0, dot(normal, halfVector));
			float spec = pow(ndoth, _SpecPower) * _SpecColour;*/

			//float4 directDiffuse = float4((ndotl * _LightColor0).rgb, 1.0);

			float4 textureA = tex2D(_MainTex, uvwA.xy) * uvwA.z;
			float4 textureB = tex2D(_MainTex, uvwB.xy) * uvwB.z;

			float4 final = _Colour * (textureA + textureB);

			return final;
		}

		ENDCG

		}
	}
}
