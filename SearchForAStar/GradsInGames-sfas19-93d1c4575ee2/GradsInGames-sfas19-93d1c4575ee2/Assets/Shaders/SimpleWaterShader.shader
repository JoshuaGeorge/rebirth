﻿Shader "Custom/SimpleWaterShader" {
	Properties{
		_Color("Colour", Color) = (1,1,1,1)
		_EdgeColor("Edge Colour", Color) = (1,1,1,1)
		_DepthFactor("Depth Factor", float) = 1.0
		_WaveSpeed("Wave Speed", float) = 1.0
		_WaveAmp("Wave Amplitude", float) = 1.0
		_NoiseTex("Noise Texture", 2D) = "white" {}
		_WaterTransparency("Water Transparency", Range(0, 1)) = 1.0
		_WaterFoamRampTex("Water Foam Ramp", 2D) = "white" {}
	}
	SubShader{
		Tags
		{
			"Queue" = "Transparent"
		}

		Pass{
		Blend SrcAlpha OneMinusSrcAlpha
		CGPROGRAM

#include "UnityCG.cginc"

#pragma vertex vert
#pragma fragment frag

		// Unity built in - NOT required in properties
		sampler2D _CameraDepthTexture;


		float4 _Color;
		float4 _EdgeColor;
		float _DepthFactor;

		float _WaveSpeed;
		float _WaveAmp;
		sampler2D _NoiseTex;

		float _WaterTransparency;
		sampler2D _WaterFoamRampTex;

		struct appdata {
			float4 vertex : POSITION;
			float2 texCoord : TEXCOORD0;
		};

		struct v2f {
			float4 pos : SV_POSITION;
			float4 screenPos : TEXCOORD1;
		};

		v2f vert(appdata i) {
			v2f o;
			// Convert our vertex local space to world space coordinates.
			o.pos = UnityObjectToClipPos(i.vertex);

			// wave animation
			float noiseSample = tex2Dlod(_NoiseTex, float4(i.texCoord.xy, 0, 0));

			o.pos.y += sin(_Time*_WaveSpeed*noiseSample)*_WaveAmp;
			o.pos.x += cos(_Time*_WaveSpeed*noiseSample)*_WaveAmp;

			// Computes textures coordinate. Used for doing screen-space texture sampling.
			o.screenPos = ComputeScreenPos(o.pos);

			
			return o;
		}

		float4 frag(v2f i) : COLOR{
			// gets the depth value (greyscale colour) from our camera depth render texture where our water plane pixels are.
			float4 depthSample = SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, i.screenPos);

			// gives high precision value from depth texture, returns eye space depth value from 0 to 1.
			float depth = LinearEyeDepth(depthSample).r;

			// get the depth value relative to our water position rather than our camera position.
			float foamLine = 1 - saturate(_DepthFactor * (depth - i.screenPos.w));

			float4 foamRamp = float4(tex2D(_WaterFoamRampTex, float2(foamLine, 0.5)).rgb, 1.0);

			float4 colour = _Color + foamRamp * _EdgeColor;

			return float4(colour.xyz, _WaterTransparency);
		}

		ENDCG
		}
	}
}
