﻿Shader "Custom/ToonShader" {
	Properties{
		_MainTex("Albedo", 2D) = "wihte" {}
		_Tint("Tint", Color) = (0,0,0,0)
		_OutlineColor("Outine Colour", Color) = (0,0,0,1)
		_OutlineWidth("Outline Width", Range(0, 10)) = 1
	}
		SubShader{

			Tags { "RenderType" = "Opaque"
					"LightMode" = "ForwardBase"
					}

			LOD 200

			// Lighting/Shading Pass
			Pass{

			Stencil
			{
				Ref 4
				Comp always
				Pass replace
				ZFail keep
			}

			CGPROGRAM
	#pragma vertex vert
	#pragma fragment frag
			#include "UnityCG.cginc"
			#include "AutoLight.cginc"
			#include "Lighting.cginc"

			sampler2D _MainTex;
		float4 _Tint;

		struct appdata
		{
			float4 vertex : POSITION;
			float3 normal: NORMAL;
			float2 uv : TEXCOORD0;
		};

		struct v2f
		{
			float4 pos : SV_POSITION;
			float2 uv : TEXCOORD0;
			float3 normal : NORMAL;
			float3 viewDir : TEXCOORD1;
		};

	v2f vert(appdata v) {
		v2f o;

		o.pos = UnityObjectToClipPos(v.vertex);
		o.uv = v.uv;
		o.normal = UnityObjectToWorldNormal(v.normal);
		o.viewDir = normalize(UnityWorldSpaceViewDir(mul(unity_ObjectToWorld, v.vertex)));
		return o;
	}

	float4 frag(v2f i) : SV_Target{
		float3 normal = normalize(i.normal);
		float ndotl = dot(normal, _WorldSpaceLightPos0);

		float3 directDiffuse = ndotl * _LightColor0;
		float3 indirectDiffuse = unity_AmbientSky;

		fixed4 col = tex2D(_MainTex, i.uv) * _Tint;
		col.rgb *= directDiffuse + indirectDiffuse;
		col.a = 1.0;

		return col;
	}


		ENDCG
		}
			// Outline Pass
			Pass{

			Cull OFF
			ZWrite OFF
			ZTest ON

			Stencil
			{
				Ref 4
				Comp notequal
				Fail keep
				Pass replace
			}

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			float4 _OutlineColor;
			float _OutlineWidth;

		struct appdata {
			float4 pos : POSITION;
			float2 uv : TEXCOORD0;
			float3 normal : NORMAL;
		};

		struct v2f {
			float4 pos : SV_POSITION;
		};

		v2f vert(appdata v) {
			v2f o;

			float4 newPos = v.pos;

			// normal extrusion technique
			float3 normal = normalize(v.normal);
			newPos += float4(normal, 0.0) * _OutlineWidth;

			// convert to world space
			o.pos = UnityObjectToClipPos(newPos);
			return o;
		}

		float4 frag(v2f i) : SV_Target{

			return _OutlineColor;
		}

			ENDCG

		}

		}
}



