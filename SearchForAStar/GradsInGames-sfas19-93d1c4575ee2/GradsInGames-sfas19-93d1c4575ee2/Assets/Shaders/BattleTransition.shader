﻿Shader "Custom/ScreemFade" {
	Properties {
		_Color("Colour", Color) = (1, 1, 1, 1)
		_MainTex("Main Texture", 2D) = "white" {}
		_TransitionTex("Transition Texture", 2D) = "white" {}
		_Cutoff("Cutoff", Range(0, 1)) = 0
	}
	SubShader {
			// No culling or depth
			Cull Off ZWrite Off ZTest Always

			Pass {

				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"

				sampler2D _MainTex;
				sampler2D _TransitionTex;
				float _Cutoff;
				float4 _Color;

				struct appdata {
					float4 pos : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct v2f {
					float4 pos : SV_POSITION;
					float2 uv : TEXCOORD0;
				};


				v2f vert(appdata i) {
					v2f o;
					o.pos = UnityObjectToClipPos(i.pos);
					o.uv = i.uv;
					return o;
				}

				fixed4 frag(v2f i) : COLOR{

					float4 color = float4(tex2D(_TransitionTex, i.uv).rgb, 1.0f);
					float3 tex = tex2D(_MainTex, i.uv);

					return (color.b < _Cutoff) ? _Color : fixed4(tex, 0.0);
				}
				ENDCG
		}
		
	}
}
