﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Transition" {
	Properties{
		_MainTex("TransitionTexture", 2D) = "white" {}
		_Cutoff("Cutoff", Range(0, 1)) = 0.0
	}

	SubShader {
			// No culling or depth
			Cull Off ZWrite Off ZTest Always

		Pass {
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

#include "UnityCG.cginc"

			sampler _MainTex;
			float _Cutoff;

			struct appdata {
				float4 pos : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f {
				float2 uv : TEXCOORD1;
				float4 pos : SV_POSITION;
			};

			v2f vert(appdata input) {
				v2f o;
				o.pos = UnityObjectToClipPos(input.pos);
				o.uv = input.uv;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 tex = tex2D(_MainTex, i.uv);
			return (tex.r > _Cutoff) ? fixed4(1.0, 1.0, 1.0, 1.0) : fixed4(0.0, 0.0, 0.0, 0.0);
			}

			ENDCG

		}
	}
}