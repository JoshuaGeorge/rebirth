﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[System.Serializable]
public struct TweenPosition
{
    public Transform trans;
    public float transitionTime;
    public float cooldownTime;
    public AnimationCurve curve;
}

public class PositionTweener : MonoBehaviour {

    public enum TweenType
    {
        Normal,
        PingPong,
        Cycle
    }

    public TweenPosition startMarker;
    public TweenPosition[] midMarkers;
    public TweenPosition endMarker;
    public TweenType type;
    public bool startOnAwake = false;
    public UnityEvent OnTweenEnd;

    private TweenJob currentJob = null;

    // Use this for initialization
    void Start () {
        transform.position = startMarker.trans.position;

        if (startOnAwake) StartTween();
    }

    public void StartTween()
    {
        if(currentJob == null ? true : currentJob.state == TweenJob.TweenState.Finished || currentJob.state == TweenJob.TweenState.None)
        {
            switch (type)
            {
                case TweenType.Normal:
                    {
                        TweenPosition[] markers = new TweenPosition[midMarkers.Length + 2];
                        markers[0] = startMarker;
                        if(midMarkers.Length > 0) midMarkers.CopyTo(markers, 1);
                        markers[midMarkers.Length + 1] = endMarker;
                        currentJob = new TweenJob(transform, markers);
                        break;
                    }
                case TweenType.PingPong:
                    {
                        TweenPosition[] markers = new TweenPosition[(midMarkers.Length * 2) + 3];
                        markers[0] = startMarker;
                        if(midMarkers.Length > 0) midMarkers.CopyTo(markers, 1);
                        markers[midMarkers.Length + 1] = endMarker;
                        int currentMarkersIndex = midMarkers.Length + 2;
                        for (int i = midMarkers.Length - 1; i >= 0; i--, currentMarkersIndex++)
                        {
                            markers[currentMarkersIndex] = midMarkers[i];
                        }
                        markers[(midMarkers.Length * 2) + 2] = startMarker;
                        currentJob = new TweenJob(transform, markers);
                        break;
                    }
                case TweenType.Cycle:
                    {
                        TweenPosition[] markers = new TweenPosition[midMarkers.Length + 3];
                        markers[0] = startMarker;
                        if(midMarkers.Length > 0) midMarkers.CopyTo(markers, 1);
                        markers[midMarkers.Length + 1] = endMarker;
                        markers[midMarkers.Length + 2] = startMarker;
                        currentJob = new TweenJob(transform, markers);
                        break;
                    }
            }
        }

    }

    public void TweenBackwards()
    {
        if (currentJob == null ? true : currentJob.state == TweenJob.TweenState.Finished)
        {
            TweenPosition[] markers = new TweenPosition[midMarkers.Length + 2];
            markers[0] = endMarker;
            for (int i = midMarkers.Length - 1, j = 1; i >= 0; i--, j++)
            {
                markers[j] = midMarkers[i];
            }
            markers[midMarkers.Length + 1] = startMarker;
            currentJob = new TweenJob(transform, markers);
        }
        
    }

    public void ResetPosition() {
        transform.position = startMarker.trans.position;
        currentJob.state = TweenJob.TweenState.Finished;
    }

    void Update()
    {
        if (currentJob == null) return;

        switch (currentJob.state)
        {
            case TweenJob.TweenState.None:
                break;
            case TweenJob.TweenState.Finished:
                currentJob.state = TweenJob.TweenState.None;
                TweenFinish();
                break;
            default:
                currentJob.Update();
                break;

        }
        
    }

    public void TweenFinish()
    {

        if(OnTweenEnd != null)
        {
            OnTweenEnd.Invoke();
        }
    }

	
}

public class TweenJob
{
    public enum TweenState
    {
        None,
        Tweening,
        Cooldown,
        Finished
    }

    private Transform transform;
    private TweenPosition[] markers;

    public TweenState state;
    private int currentTweenIndex = 0;
    private float currentTransitionTime, currentCooldownTime;
    private float nextTransitionTime = 1.0f;
    private float cooldownTime = 1.0f;
    private Vector3 startPos, targetPos;
    private AnimationCurve currentCurve;


    public TweenJob(Transform transform, TweenPosition[] markers)
    {
        this.transform = transform;
        this.markers = markers;
        targetPos = markers[0].trans.position;
        NextTween();
    }

    private void NextTween()
    {
        if(currentTweenIndex == markers.Length - 1)
        {
            state = TweenState.Finished;
            return;
        }

        SetNextTween(targetPos, markers[++currentTweenIndex]);
        state = TweenState.Tweening;
    }

    private void SetNextTween(Vector3 currentPosition, TweenPosition nextPosition)
    {
        startPos = currentPosition;
        targetPos = nextPosition.trans.position;
        nextTransitionTime = nextPosition.transitionTime;
        cooldownTime = nextPosition.cooldownTime;
        currentCurve = nextPosition.curve;
    }
    private void BeginCooldown()
    {
        currentTransitionTime = 0;
        currentCooldownTime = 0;
        state = TweenState.Cooldown;
    }

    public void Update()
    {
        switch (state)
        {
            case TweenState.None:
                break;
            case TweenState.Tweening:
                if (currentTransitionTime < nextTransitionTime)
                {
                    UpdateTween();
                }
                else
                {
                    BeginCooldown();
                }
                break;
            case TweenState.Cooldown:
                if (currentCooldownTime < cooldownTime)
                {
                    UpdateCooldown();
                    break;
                }
                else
                {
                    NextTween();
                }
                break;
        }
    }

    void UpdateTween()
    {
        currentTransitionTime += Time.deltaTime;
        float frac = (currentTransitionTime / nextTransitionTime);
        transform.position = Vector3.Lerp(startPos, targetPos, frac * currentCurve.Evaluate(frac));
    }

    void UpdateCooldown()
    {
        currentCooldownTime += Time.deltaTime;
    }


}
