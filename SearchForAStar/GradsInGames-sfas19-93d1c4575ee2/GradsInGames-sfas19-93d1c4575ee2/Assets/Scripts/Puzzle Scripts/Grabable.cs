﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ConfigurableJoint))]
public class Grabable : MonoBehaviour {

    private Rigidbody rb;
    private ConfigurableJoint joint;
    private float mass;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        joint = GetComponent<ConfigurableJoint>();

        mass = rb.mass;
    }
	
	public void Grab(Rigidbody actor)
    {
        joint.connectedBody = actor;
    }

    public void Release()
    {
        joint.connectedBody = null;
    }

    public float GetMass()
    {
        return mass;
    }
}
