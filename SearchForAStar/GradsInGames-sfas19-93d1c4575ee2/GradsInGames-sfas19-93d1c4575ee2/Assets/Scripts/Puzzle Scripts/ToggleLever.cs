﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleLever : MonoBehaviour {

    public GameObject switchPivot;

    public float onRotation = -15.0f;

    public float offRotation = 15.0f;

    private bool turnedOn = false;

    public void Toggle()
    {
        switchPivot.transform.rotation = Quaternion.Euler(0, 0, (turnedOn ? offRotation : onRotation));
        turnedOn = !turnedOn;
    }
}
