﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Grabber : MonoBehaviour {

    public float interactDistance = 1.0f;
    public float maxWeight = 200.0f;
    public float maxSpeed = 5.0f;
    public PlayerController player;
    public Animator anim;

    private Rigidbody rb;

    private Grabable currentGrabable = null;

    private int ANIM_PUSH = Animator.StringToHash("IsPushing");

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}

    private void Update()
    {
        Debug.DrawLine(transform.position, transform.position + transform.forward, Color.green);

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            anim.SetBool(ANIM_PUSH, true);
            SetPlayerCanRotate(false);
            currentGrabable = TryGrab();
            SetPlayerSpeed();
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            SetPlayerCanRotate(true);
            if (currentGrabable)
            {
                anim.SetBool(ANIM_PUSH, false);
                currentGrabable.Release();
                SetPlayerSpeed();
            }
        }
    }

    Grabable TryGrab()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit, interactDistance, ~(1 << 2)))
        {
            Grabable grab = hit.transform.gameObject.GetComponent<Grabable>();

            if (grab)
            {
                Debug.Log("Grabbed");
                grab.Grab(rb);
                return grab;
            }
        }

        return null;
    }

    void SetPlayerSpeed()
    {
        if (!currentGrabable) { player.SetSpeed(maxSpeed); return; }

        //float remainingStrength = 1 - (currentGrabable.GetMass() / maxWeight);
        //player.SetSpeed(maxSpeed * remainingStrength);
    }

    void SetPlayerCanRotate(bool canRotate)
    {
        player.canRotate = canRotate;
    }
	
}
