﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeTransition : MonoBehaviour
{

    public Color colour;
    public Texture transitionTexture;
    public Texture transitionToGameTexture;

    public float transitionSpeed = 1.0f;

    private Material mat;

    private void Start()
    {
        mat = new Material(Shader.Find("Custom/ScreemFade"));
        mat.SetTexture("_TransitionTex", transitionTexture);
        mat.SetColor("_Color", colour);
    }


    IEnumerator TransitionToBlack(float speed, bool distort)
    {
        mat.SetTexture("_TransitionTex", transitionTexture);
        mat.SetFloat("_Fade", 0);
        mat.SetFloat("_Distort", (distort) ? 1 : 0);

        float cutoff = 0;

        while (cutoff < 1)
        {
            cutoff += Time.smoothDeltaTime * speed;
            mat.SetFloat("_Cutoff", cutoff);
            mat.SetFloat("_Fade", cutoff);
            yield return null;
        }

    }

    IEnumerator TransitionToGame(float speed)
    {
        mat.SetTexture("_TransitionTex", transitionToGameTexture);
        mat.SetFloat("_Fade", 0);

        float cutoff = 1;

        while (cutoff > 0)
        {
            cutoff -= Time.smoothDeltaTime * speed;
            mat.SetFloat("_Cutoff", cutoff);
            yield return null;
        }

    }

    [ContextMenu("Play Battle Transition")]
    private void Play()
    {
        if (mat == null)
        {
            Debug.LogError("Must be in play mode!");
            return;
        }

        StartCoroutine(TransitionToBlack(1, false));
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (mat)
        {
            Graphics.Blit(source, null, mat);
        }
    }
}
