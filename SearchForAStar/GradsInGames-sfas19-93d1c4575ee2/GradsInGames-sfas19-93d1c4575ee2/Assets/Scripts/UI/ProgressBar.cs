﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class ProgressBar : MonoBehaviour
{

    public GameObject progressBarUI;

    public Image progressImage;

    public Gradient gradient;

    protected virtual void Start()
    {
        // init health wheel to 100%
        progressImage.color = gradient.Evaluate(1);
        progressImage.fillAmount = 1;
    }

    protected virtual void UpdateProgressBar(float percentage)
    {
        progressImage.color = gradient.Evaluate(percentage);
        progressImage.fillAmount = percentage;
    }

    protected void ToggleUI(bool show)
    {
        progressBarUI.SetActive(show);
    }

}
