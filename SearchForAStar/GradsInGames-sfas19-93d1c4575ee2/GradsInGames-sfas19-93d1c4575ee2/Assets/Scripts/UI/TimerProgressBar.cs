﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerProgressBar : ProgressBar {

    public Timer timer;

    protected override void Start()
    {
        base.Start();

        ToggleUI(false);

        SubscribeEvents();
    }

    private void OnDestroy()
    {
        if (timer == null) return;

        timer.UnsubscribeOnBegin(OnTimerStarted);
        timer.UnsubscribeOnProgressChanged(OnTimerChanged);
        timer.UnsubscribeOnTimerOver(OnTimerEnded);
    }

    private void SubscribeEvents()
    {
        if (timer == null) return;

        timer.SubscribeOnBegin(OnTimerStarted);
        timer.SubscribeOnProgressChanged(OnTimerChanged);
        timer.SubscribeOnTimerOver(OnTimerEnded);
        timer.SubscribeOnEndedEarly(OnTimerEnded);
    }

    private void OnTimerChanged()
    {
        if (timer == null) return;

        float progress01 = timer.GetProgress01();

        UpdateProgressBar(progress01);
    }

    private void OnTimerStarted()
    {
        if (timer == null) return;

        ToggleUI(true);
    }

    private void OnTimerEnded()
    {
        if (timer == null) return;

        ToggleUI(false);
    }

}
