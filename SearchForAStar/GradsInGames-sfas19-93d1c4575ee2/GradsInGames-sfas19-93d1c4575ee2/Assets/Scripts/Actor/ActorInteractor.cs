﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Puzzles.Interactables;
public class ActorInteractor : MonoBehaviour {

    public float interactDistance = 1.0f;
    public KeyCode key;
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(key))
        {
            TryInteract();
        }
	}

    bool TryInteract()
    {
        RaycastHit hit;

        if(Physics.Raycast(transform.position, transform.forward, out hit, interactDistance, ~(1 << 2)))
        {
            InteractableButton interactableHit = hit.transform.GetComponent<InteractableButton>();

            if (interactableHit != null)
            {
                interactableHit.Interact(this);
            }

            return false;
        }

        return false;
    }



}
