﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObjects : MonoBehaviour {

    [System.Serializable]
    public struct SpawnObject
    {
        public Transform transform;
        public GameObject obj;
        public float delay;
    }

    public SpawnObject[] objects;


    public void SpawnNewObjects()
    {
        foreach(SpawnObject o in objects)
        {
            Spawn(o);
        }
    }

    void Spawn(SpawnObject spawnObject)
    {
        Instantiate(spawnObject.obj, spawnObject.transform.position, Quaternion.identity);
    }
}
