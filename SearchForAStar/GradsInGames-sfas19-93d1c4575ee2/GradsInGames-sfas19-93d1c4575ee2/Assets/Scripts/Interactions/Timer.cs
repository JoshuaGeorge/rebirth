﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour {

    public float timerDuration;

    private float currentProgress;
    private bool timerStarted, paused;

    public UnityEvent TimerOverEvent;

    public UnityAction OnTimerOver;
    public UnityAction OnTimerBegin;
    public UnityAction OnProgressChanged;
    public UnityAction OnEndedEarly;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        // if we have a timer going and we are paused then don't do any calculations.
        if (timerStarted && paused) return;

        // if we have started a timer and the duration is less than 0, then end the timer
        if(timerStarted && currentProgress <= 0)
        {
            OnTimerEnd();
        }
        else if (currentProgress > 0)
        {
            currentProgress -= Time.deltaTime;
            if(OnProgressChanged != null) OnProgressChanged();
        }	
	}

    public void SubscribeOnTimerOver(UnityAction onTimerOver)
    {
        OnTimerOver += onTimerOver;
    }

    public void UnsubscribeOnTimerOver(UnityAction onTimerOver)
    {
        OnTimerOver -= onTimerOver;
    }

    public void SubscribeOnEndedEarly(UnityAction onEndedEarly)
    {
        OnEndedEarly += onEndedEarly;
    }

    public void UnsubscribeOnEndedEarly(UnityAction onEndedEarly)
    {
        OnEndedEarly -= onEndedEarly;
    }

    public void SubscribeOnBegin(UnityAction onTimerBegin)
    {
        OnTimerBegin += onTimerBegin;
    }

    public void UnsubscribeOnBegin(UnityAction onTimerBegin)
    {
        OnTimerBegin -= onTimerBegin;
    }

    public void SubscribeOnProgressChanged(UnityAction onProgressChanged)
    {
        OnProgressChanged += onProgressChanged;
    }

    public void UnsubscribeOnProgressChanged(UnityAction onProgressChanged)
    {
        OnProgressChanged -= onProgressChanged;
    }

    public void StartTimer(float duration)
    {
        timerDuration = duration;
        RestartTimer();
    }

    public void RestartTimer()
    {
        paused = false;
        timerStarted = true;
        currentProgress = timerDuration;
        if(OnTimerBegin != null) OnTimerBegin();
    }

    public void EndTimerEarly(bool withEndEarlyEvent)
    {
        if (withEndEarlyEvent)
        {
            if (OnEndedEarly != null)
                OnEndedEarly();
        }

        timerStarted = false;
        currentProgress = 0.0f;
        if (OnProgressChanged != null) OnProgressChanged();
    }

    public void DecreaseProgress(float seconds)
    {
        currentProgress = Mathf.Min(currentProgress - seconds, 0);
        if (OnProgressChanged != null) OnProgressChanged();
    }

    public void IncreaseProgress(float seconds)
    {
        currentProgress = Mathf.Max(currentProgress + seconds, timerDuration);
        if (OnProgressChanged != null) OnProgressChanged();
    }

    public void PauseTimer()
    {
        paused = true;
    }

    public void ResumeTimer()
    {
        paused = false;
    }

    private void OnTimerEnd()
    {
        timerStarted = false;
        if(OnTimerOver != null) OnTimerOver();
        if (TimerOverEvent != null) TimerOverEvent.Invoke();
    }

    public float GetTimerPercentage()
    {
        return (currentProgress / timerDuration) * 100.0f;
    }

    public float GetProgress01()
    {
        return GetTimerPercentage() / 100.0f;
    }

    public bool IsRunning()
    {
        return timerStarted && !paused;
    }

    public bool IsPaused()
    {
        return paused;
    }
}
