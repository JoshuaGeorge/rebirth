﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Puzzles.Interactables
{

    public class Button : MonoBehaviour, IInteractable
    {

        public UnityEvent OnPressedEvent;
        public UnityEvent OnReleasedEvent;

        public bool canBeToggled = false;

        protected bool isPressedDown = false;

        public virtual void OnPressed()
        {
            isPressedDown = true;
            OnPressedEvent.Invoke();
        }

        public virtual void OnReleased()
        {
            isPressedDown = false;
            OnReleasedEvent.Invoke();
        }

        public virtual bool CanBePressed()
        {
            return !isPressedDown || canBeToggled;
        }
    }
}