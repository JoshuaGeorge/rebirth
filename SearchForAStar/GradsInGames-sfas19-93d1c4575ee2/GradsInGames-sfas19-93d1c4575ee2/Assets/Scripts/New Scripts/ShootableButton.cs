﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Puzzles.Interactables
{

    public class ShootableButton : Button {

        private void Start()
        {
            // shootable buttons can't be toggled
            canBeToggled = false;
        }

        public void Shoot()
        {
            if (CanBePressed())
            {
                OnPressed();
            }
        }
    }
}
