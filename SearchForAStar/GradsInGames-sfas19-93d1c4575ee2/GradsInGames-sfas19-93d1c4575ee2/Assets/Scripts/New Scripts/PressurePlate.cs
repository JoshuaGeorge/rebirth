﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Puzzles.Interactables
{

    public class PressurePlate : MonoBehaviour, IInteractable
    {
        public UnityEvent OnPressedEvent;
        public UnityEvent OnReleasedEvent;

        private bool isPressedDown = false;

        public void PressPlate()
        {
            if (!isPressedDown)
            {
                OnPressed();
            }
        }

        public void OnPressed()
        {
            isPressedDown = true;
            OnPressedEvent.Invoke();
        }

        public void OnReleased()
        {
            isPressedDown = false;
            OnReleasedEvent.Invoke();
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<PressureWeight>())
            {
                PressPlate();
            }
        }

        void OnTriggerExit(Collider other)
        {
            if (other.GetComponent<PressureWeight>())
            {
                OnReleased();
            }
        }

    }
}
