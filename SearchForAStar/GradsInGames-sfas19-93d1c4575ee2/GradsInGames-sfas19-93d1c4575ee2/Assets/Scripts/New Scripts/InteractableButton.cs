﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Puzzles.Interactables
{

    public class InteractableButton : Button
    {
        public float maxLookAngle = 100.0f;

        public void Interact(ActorInteractor actor)
        {
            if (CheckAngle(actor.transform.forward) && CanBePressed())
            {
                // if this button can be toggled and it is currently pressed down, then release the button
                if (canBeToggled && isPressedDown)
                {
                    OnReleased();
                }
                else
                {
                    OnPressed();
                }
            }

        }

        bool CheckAngle(Vector3 actorLookDir)
        {
            float dot = Vector3.Dot(-actorLookDir.normalized, transform.forward.normalized);
            float angle = Mathf.Rad2Deg * Mathf.Acos(Mathf.Abs(dot));

            return (angle < maxLookAngle);
        }
    }
}
