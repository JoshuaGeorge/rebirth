﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PatrolAIState : AIState
{

    int priority;

    float patrolSpeed;

    private int minChangeDirectionTime = 10;
    private int maxChangeDirectionTime = 30;
    private Transform[] patrolPoints;
    NavMeshAgent navMesh;
    float timeTillCheckpointChange;

    public PatrolAIState(BasicAI controller, float speed, int minChangeDirectionTime, int maxChangeDirectionTime, Transform[] patrolPoints, float timeTillCheckpointChange, int priority = 1)
    {
        navMesh = controller.GetNavAgent();
        patrolSpeed = speed;
        this.minChangeDirectionTime = minChangeDirectionTime;
        this.maxChangeDirectionTime = maxChangeDirectionTime;
        this.patrolPoints = patrolPoints;
        this.timeTillCheckpointChange = timeTillCheckpointChange;
        this.priority = priority;
    }

    public int GetPriority()
    {
        return priority;
    }

    public void OnEnter()
    {
        navMesh.speed = patrolSpeed;
        navMesh.SetDestination(GetRandomCheckpoint());
        timeTillCheckpointChange = Random.Range(minChangeDirectionTime, maxChangeDirectionTime);
    }

    public void OnExit()
    {
        navMesh.speed = 0.0f;
    }

    public void OnUpdate()
    {
        Patrolling();

        ChangeCheckpointAtRandomTime();
    }

    private void Patrolling()
    {
        if (navMesh.remainingDistance <= navMesh.stoppingDistance)
        {
            navMesh.SetDestination(GetRandomCheckpoint());
        }
    }

    private Vector3 GetRandomCheckpoint()
    {
        int rand = Random.Range(0, patrolPoints.Length - 1);
        return patrolPoints[rand].position;
    }

    private void ChangeCheckpointAtRandomTime()
    {
        timeTillCheckpointChange -= Time.deltaTime;

        if (timeTillCheckpointChange <= 0)
        {
            navMesh.SetDestination(GetRandomCheckpoint());
        }
    }
}
