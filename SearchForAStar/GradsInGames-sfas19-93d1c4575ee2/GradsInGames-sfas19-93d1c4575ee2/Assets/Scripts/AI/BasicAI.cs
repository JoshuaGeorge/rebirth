﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class BasicAI : MonoBehaviour
{

    protected AIState currentState;
    private NavMeshAgent navMeshAgent;

    protected void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    protected virtual void Update()
    {
        if (currentState != null)
        {
            currentState.OnUpdate();
        }
    }

    public NavMeshAgent GetNavAgent()
    {
        return navMeshAgent;
    }

    public void SwitchState(AIState nextState)
    {
        // if we are already in this state, then return without switching
        if (currentState == nextState) return;

        if(currentState != null)
        {
            currentState.OnExit();
        }
        if (StateHasPriority(currentState, nextState))
        {
            Debug.Log("Switching to state: " + nextState.ToString());
            nextState.OnEnter();
            currentState = nextState;
        }
    }

    bool StateHasPriority(AIState currentState, AIState nextState)
    {
        int currentStatePriority = (currentState != null) ? currentState.GetPriority() : 0;
        int nextstatePriority = (nextState != null) ? nextState.GetPriority() : 0;
        return nextstatePriority >= currentStatePriority;
    }
}
