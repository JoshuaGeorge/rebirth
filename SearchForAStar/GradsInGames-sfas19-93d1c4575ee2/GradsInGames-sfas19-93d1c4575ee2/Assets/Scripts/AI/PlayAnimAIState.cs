﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAnimAIState : AIState {

    public enum AnimType
    {
        Trigger,
        Bool
    }

    private Animator anim;

    private int ANIM_HASH;

    private AnimType type;

    private int priority;

    public PlayAnimAIState(AnimatedAI controller, int animationHash, AnimType animationType, int priority = 1)
    {
        anim = controller.Animator;
        ANIM_HASH = animationHash;
        type = animationType;
        this.priority = priority;
    } 

    public void OnEnter()
    {
        switch (type)
        {
            case AnimType.Bool:
                anim.SetBool(ANIM_HASH, true);
                break;
            case AnimType.Trigger:
                anim.SetTrigger(ANIM_HASH);
                break;
        }
    }

    public void OnUpdate()
    {
    }

    public void OnExit()
    {
        if(type == AnimType.Bool)
        {
            anim.SetBool(ANIM_HASH, false);
        }
    }

    public int GetPriority()
    {
        return priority;
    }
}
