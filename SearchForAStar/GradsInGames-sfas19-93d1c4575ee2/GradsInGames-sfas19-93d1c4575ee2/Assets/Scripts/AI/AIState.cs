﻿
public interface AIState
{
    void OnEnter();
    void OnUpdate();
    void OnExit();
    int GetPriority();

}