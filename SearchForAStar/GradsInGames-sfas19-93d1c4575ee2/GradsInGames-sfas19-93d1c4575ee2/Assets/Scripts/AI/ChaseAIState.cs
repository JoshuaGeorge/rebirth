﻿using UnityEngine;
using UnityEngine.AI;
public class ChaseAIState : AIState
{
    int priority;

    private NavMeshAgent navMesh;

    float fieldOfView;

    float maxChaseDistance;

    float chaseSpeed;

    Transform aiTransform;
    Transform targetTransform;

    public ChaseAIState(BasicAI controller, float fov, float maxChaseDistance, float speed, Transform ai, Transform target, int priority = 1)
    {
        navMesh = controller.GetNavAgent();
        fieldOfView = fov;
        this.maxChaseDistance = maxChaseDistance;
        aiTransform = ai;
        targetTransform = target;
        chaseSpeed = speed;
        this.priority = priority;
    }

    public void SetNewTarget(Transform newTarget)
    {
        targetTransform = newTarget;
    }

    public int GetPriority()
    {
        return priority;
    }

    public void OnEnter()
    {
        navMesh.speed = chaseSpeed;
    }

    public void OnExit()
    {
        navMesh.speed = 0.0f;
    }

    public void OnUpdate()
    {
        if (targetTransform == null) OnExit();

        bool playerInSight = false;

        Vector3 direction = targetTransform.position - aiTransform.position;
        float angle = Vector3.Angle(direction, aiTransform.forward);
        if (angle < fieldOfView)
        {
            RaycastHit hit;

            if (Physics.Raycast(aiTransform.position + (aiTransform.up / 2), direction.normalized * maxChaseDistance, out hit))
            {
                if (hit.transform == targetTransform)
                {
                    playerInSight = true;
                    Debug.Log("Player in sight");
                }
            }
            Debug.DrawRay(aiTransform.position + (aiTransform.up / 2), direction.normalized * maxChaseDistance);
        }

        if (playerInSight)
        {
            Chase();
        }
        else
        {
            OnExit();
        }
    }

    void Chase()
    {
        navMesh.destination = targetTransform.position;
    }
}
