﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : AnimatedAI
{
    // --------------------------------------------------------------

    // The character's patrolling speed
    [SerializeField]
    float m_MovementSpeed = 4.0f;
    
    // The character's running speed
    [SerializeField]
    float m_ChaseSpeed = 4.0f;

    // The gravity strength
    [SerializeField]
    float m_Gravity = 60.0f;

    // The maximum speed the character can fall
    [SerializeField]
    float m_MaxFallSpeed = 20.0f;

    // The maximum speed the character can fall
    [SerializeField]
    float m_AgroDistance = 20.0f;
    
    // The distance the AI needs to be to attack the player
    [SerializeField]
    float m_AttackDistance = 2.0f;

    // The field of view the AI's sight
    [SerializeField]
    float m_FieldOfView = 80.0f;

    // Places this AI may patrol to while in the patrol state
    [SerializeField]
    Transform[] m_PatrolPoints;

    // Number of death animations this AI has
    [SerializeField]
    int m_NumberOfDeathAnimations = 3;

    // The minimum time for this AI to be idling
    [SerializeField]
    float m_minIdleTime = 10.0f;

    // The maximum time for this AI to be idling
    [SerializeField]
    float m_maxIdleTime = 30.0f;

    // --------------------------------------------------------------

    // The charactercontroller of the player
    CharacterController m_CharacterController;

    // The current movement direction in x & z.
    Vector3 m_MovementDirection = Vector3.zero;

    // The current vertical / falling speed
    float m_VerticalSpeed = 0.0f;

    // The current movement offset
    Vector3 m_CurrentMovementOffset = Vector3.zero;

    // Whether the player is alive or not
    bool m_IsAlive = true;

    PlayerController m_PlayerController;
    Transform m_PlayerTransform;

    float m_CurrentTimeInState = 0;
    float m_ChangeStateTime = 0;

    int ANIM_DEATHANIMATION = Animator.StringToHash("DeathAnimation");

    int m_PatrolPointsCount = 0;

    // --------------------------------------------------------------

    // States

    PlayAnimAIState idleAnimationState, biteAnimationState, dieAnimationState;

    ChaseAIState chaseState;

    PatrolAIState patrolState;

    protected void Awake()
    {
        base.Awake();
        m_CharacterController = GetComponent<CharacterController>();
    }

    // Use this for initialization
    void Start()
    {
        // Get Player information
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if(player)
        {
            m_PlayerController = player.GetComponent<PlayerController>();
            m_PlayerTransform = player.transform;
        }

        m_PatrolPointsCount = m_PatrolPoints.Length;
        m_ChangeStateTime = Random.Range(m_minIdleTime, m_maxIdleTime);

        InitStates();

        SwitchState(idleAnimationState);

    }

    void InitStates()
    {
        idleAnimationState = new PlayAnimAIState(this, Animator.StringToHash("IsIdle"), PlayAnimAIState.AnimType.Bool, 1);
        biteAnimationState = new PlayAnimAIState(this, Animator.StringToHash("Bite"), PlayAnimAIState.AnimType.Trigger, 9);
        dieAnimationState = new PlayAnimAIState(this, Animator.StringToHash("Die"), PlayAnimAIState.AnimType.Trigger, 10);
        chaseState = new ChaseAIState(this, m_FieldOfView, m_AgroDistance, m_ChaseSpeed, transform, m_PlayerTransform, 3);
        patrolState = new PatrolAIState(this, m_MovementSpeed, 10, 15, m_PatrolPoints, 20.0f, 2);
    }

    void ApplyGravity()
    {
        // Apply gravity
        m_VerticalSpeed -= m_Gravity * Time.deltaTime;

        // Make sure we don't fall any faster than m_MaxFallSpeed.
        m_VerticalSpeed = Mathf.Max(m_VerticalSpeed, -m_MaxFallSpeed);
        m_VerticalSpeed = Mathf.Min(m_VerticalSpeed, m_MaxFallSpeed);
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        // If the player is dead update the respawn timer and exit update loop
        if (!m_IsAlive)
        {
            return;
        }

        float distance = Vector3.Distance(m_PlayerTransform.position, transform.position);

        // Attack range
        if (distance < m_AttackDistance && m_PlayerController.m_IsAlive)
        {
            // killplayer
            m_PlayerController.Die();

            SwitchState(biteAnimationState);
            return;
        }

        // Aggro range
        if (distance < m_AgroDistance)
        {
            SwitchState(chaseState);
            return;
        }
        else
        {
            if (m_CurrentTimeInState >= m_ChangeStateTime)
            {
                if (currentState == idleAnimationState && m_PatrolPointsCount > 0)
                {
                    ChangeToNextState(patrolState);
                    return;
                }
                else
                {
                    ChangeToNextState(idleAnimationState);
                    return;
                }
            }

            m_CurrentTimeInState += Time.deltaTime;
        }


        // Update jumping input and apply gravity
        ApplyGravity();

        /*
        // Calculate actual motion
        m_CurrentMovementOffset = (m_MovementDirection * m_MovementSpeed + new Vector3(0, m_VerticalSpeed, 0)) * Time.deltaTime;

        // Move character
        m_CharacterController.Move(m_CurrentMovementOffset);

        // Rotate the character in movement direction
        if(m_MovementDirection != Vector3.zero)
        {
            RotateCharacter(m_MovementDirection);
        }
        */
    }

    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }

    void RotateCharacter(Vector3 movementDirection)
    {
        Quaternion lookRotation = Quaternion.LookRotation(movementDirection);
        if (transform.rotation != lookRotation)
        {
            transform.rotation = lookRotation;
        }
    }

    void ChangeToNextState(AIState nextState)
    {
        m_CurrentTimeInState = 0.0f;
        m_ChangeStateTime = Random.Range(m_minIdleTime, m_maxIdleTime);
        SwitchState(nextState);
    }

    public void Die()
    {
        m_CharacterController.enabled = false;
        anim.SetInteger(ANIM_DEATHANIMATION, Random.Range(1, m_NumberOfDeathAnimations));
        SwitchState(dieAnimationState);
        m_IsAlive = false;
    }
}
