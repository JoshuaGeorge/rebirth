﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnDamageReceivedEvent : UnityEvent<int> { }

public class Health : MonoBehaviour
{
    public OnDamageReceivedEvent OnDamageReceived;
    public UnityEvent OnNoHealth;

    // The total health of this unit
    [SerializeField]
    int m_Health = 100;

    public void DoDamage(int damage)
    {
        m_Health -= damage;

        OnDamageReceived.Invoke(damage);

        if(m_Health <= 0)
        {
            OnNoHealth.Invoke();
        }
    }

    public bool IsAlive()
    {
        return m_Health > 0;
    }
}
